const walkSync = require('walk-sync');

console.log("Initialize Applications");

module.exports = function (app) {
    let apps = walkSync('./src/plugins', {globs: ['**/*edit-*']}).map(path => path.split(".js")[0]);
    app.get('/editors', (req, res) => {
        res.send(apps);
    });
};
