const gql = require("graphql-api-express");

gql.add(({query, api}) => {
    query({
        [api("version", "String")]: () => "1.0.1",
        [api("echo", ["n: Int!"], "String")]: (obj, {n}) => "" + n * 100
    });
});
