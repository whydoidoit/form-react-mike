require('dotenv').config();
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
let cors = require('cors');
let gql = require("graphql-api-express");
gql.initialize.addScanPath(`${__dirname}/server`);
gql.initialize.pattern = "**/api-*";

global.rootDir = __dirname;

let app = express();
app.use(cors({
    credentials: true,
    origin(origin, callback) {
        callback(null, true);
    }
}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
require('./server/editor-components')(app);
gql.initialize(app);

module.exports = app;
