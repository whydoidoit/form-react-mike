// WARNING: DO NOT EDIT. This file is automatically generated by AWS Amplify. It will be overwritten.

const awsmobile =  {
    "aws_project_region": "eu-west-1",
    "aws_cognito_identity_pool_id": "eu-west-1:8cf2fc9a-f00f-49a7-b6eb-26ea7bc2696f",
    "aws_cognito_region": "eu-west-1",
    "aws_user_pools_id": "eu-west-1_XhfnFr1Jg",
    "aws_user_pools_web_client_id": "2vv5im5k41p3ub25ndocooq43l",
    "aws_user_files_s3_bucket": "form20f114cd6716429f989f4eeb3c653a75",
    "aws_user_files_s3_bucket_region": "eu-west-1",
    "aws_dynamodb_all_tables_region": "eu-west-1",
    "aws_dynamodb_table_schemas": [
        {
            "tableName": "data",
            "region": "eu-west-1"
        }
    ]
};


export default awsmobile;
