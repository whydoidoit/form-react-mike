import React, {useState} from "react";
import mobiscroll from "@mobiscroll/react";
import "./forms.css";
import async from "../lib/async";
import editors, {events} from "../lib/editors";

export default function Form(props) {
    const data = async(props.data);
    if (!data) return null;
    const [name, setName] = useState(props.state[data.heading.name]);
    return data && editors() && (
        <div className="form">
            <div className="mbsc-padding">
                <h1>{name || data.heading.title}</h1>
                {data.heading.description && <div dangerouslySetInnerHTML={{__html: data.heading.description}}></div>}
            </div>
            {data.sections.map(section => {
                return (
                    <mobiscroll.FormGroup key={section.title}>
                        {section.title && <mobiscroll.FormGroupTitle>{section.title}</mobiscroll.FormGroupTitle>}
                        {section.description &&
                        <div className="section-intro" dangerouslySetInnerHTML={{__html: section.description}}></div>}
                        {section.questions.map(q => {
                            const context = {
                                ...q,
                                _editor: {
                                    reference: q,
                                    ...props,
                                    priority: 0,
                                    fn: () => <li>{q.name}</li>,
                                },
                            };
                            if (q.name === data.heading.name) {
                                context._editor.update = (value) => setName(value);
                            }
                            events.emit(`canedit.${q.type}`, context);
                            return <div key={q.name}>{context._editor.fn()}</div>;
                        })}
                    </mobiscroll.FormGroup>

                );
            })}
        </div>
    );
};
