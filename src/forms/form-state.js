import {useState} from "react";
import get from "lodash/get";
import set from "lodash/set";

export default function useFormState(context, defaultValue = "") {
    const [value, setValue] = useState(get(context._editor.state, context.name, defaultValue));
    return [
        value, (value) => {
            set(context._editor.state, context.name, value);
            setValue(value);
            context._editor.update && context._editor.update(value);
        }
    ]
}
