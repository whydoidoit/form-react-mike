import React, {useState} from 'react';
import '@mobiscroll/react/dist/css/mobiscroll.min.css';
import PageLayout from './page-layout';
import Form from './forms';

import './App.css';

function application() {
    const [state, setState] = useState({});
    return (
        <div className="form">
                <PageLayout>
                <Form labelStyle="inline" state={state} onChanged={(value)=>{setState(value);}} data={(async () => {
                    let result = await fetch('form.json');
                    return await result.json();
                })()}/>
            </PageLayout>
        </div>
    );

}

export default application;
