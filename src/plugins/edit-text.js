import {events} from "../lib/editors";
import React from "react";
import mobiscroll from "@mobiscroll/react";
import useFormState from "../forms/form-state";

events.on("canedit.text", function (context) {
    if (context._editor.priority < 1) {
        context._editor.priority = 1;
        const [value, setValue] = useFormState(context);
        context._editor.fn = () => (
            <mobiscroll.Input type="text" value={value} onChange={event => setValue(event.target.value)} placeholder={context.placeholder}>{context.question}</mobiscroll.Input>
        );
    }
});

