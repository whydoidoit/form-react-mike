import {events} from "../../lib/editors";
import React from "react";
import mobiscroll from "@mobiscroll/react";
import useFormState from "../../forms/form-state";

events.on("canedit.number", function (context) {
    if (context._editor.priority < 100 && context.subtype === "rating") {
        context._editor.priority = 100;
        const [value, setValue] = useFormState(context, 0);
        context._editor.fn = () => (
            <mobiscroll.Rating value={value} className="white-bg"  onChange={value=>setValue(+value)}>{context.question}</mobiscroll.Rating>
        );
    }
});

