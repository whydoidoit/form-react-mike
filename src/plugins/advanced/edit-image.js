import {events} from "../../lib/editors";
import React from "react";

events.on("canedit.image", function (context) {
    if (context._editor.priority < 1) {
        context._editor.priority = 1;
        context._editor.fn = () => (
            <div></div>
        );
    }
});

