import {events} from "../../lib/editors";
import React, {useState} from "react";
import get from "lodash/get";
import mobiscroll from "@mobiscroll/react";
import set from "lodash/set";

events.on("canedit.number", function (context) {
    if (context._editor.priority < 10) {
        context._editor.priority = 10;
        context.max = context.max || 0;
        context.min = context.min || 0;
        const [value, setValue] = useState(get(context._editor.state, context.name, ""));
        context._editor.fn = () => (
            <mobiscroll.Numpad
                preset="decimal"
                onSet={(event, instance)=>{
                    let value = instance.getVal();
                    set(context._editor.state, context.name, value);
                    setValue(value);
                }}
                min={context.min || 0}
                scale={context.decimalPlaces || (context.min !== Math.floor(context.min) || context.max !== Math.floor(context.max) ? 2 : 0)}
                max={context.max || 100000000}
            >
                <mobiscroll.Input labelStyle={context._editor.labelStyle} value={value} onChange={event => {
                    let value = event.target.value;
                    set(context._editor.state, context.name, value);
                    setValue(value);
                }} placeholder={context.placeholder}>{context.question}</mobiscroll.Input>

            </mobiscroll.Numpad>
        );
    }
});
