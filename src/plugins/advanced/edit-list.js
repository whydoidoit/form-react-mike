import {events} from "../../lib/editors";
import React, {useState} from "react";
import get from "lodash/get";
import mobiscroll from "@mobiscroll/react";
import set from "lodash/set";
import isArray from "lodash/isArray";

events.on("canedit.list", function (context) {
    if (context._editor.priority < 10) {
        context._editor.priority = 10;
        if(isArray(context.list)) {
            const [value, setValue] = useState(get(context._editor.state, context.name, ""));
            context._editor.fn = () => (
                <mobiscroll.Dropdown
                    label={context.question}
                    labelStyle={context._editor.labelStyle}
                    value={value}
                    onChange={event => {
                        let value = event.target.value;
                        set(context._editor.state, context.name, value);
                        setValue(value);
                        console.log(context._editor.state);
                    }}
                >
                    {context.placeholder && <option key="">{context.placeholder}</option>}
                    {context.list.map(item => {
                        return <option key={item.value} value={item.value}>{item.text}</option>;
                    })}
                </mobiscroll.Dropdown>
            );
        } else {
            context._editor.fn = ()=><div></div>
        }
    }
});
