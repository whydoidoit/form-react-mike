import {events} from "../../lib/editors";
import React, {useState} from "react";
import get from "lodash/get";
import set from "lodash/set";
import mobiscroll from "@mobiscroll/react";

events.on("canedit.group", function (context) {
    if (context._editor.priority < 1) {
        context._editor.priority = 1;
        const [value, setValue] = useState(get(context._editor.state, context.name));
        context._editor.fn = () => (
            <div></div>
        );
    }
});

