import {events} from "../../lib/editors";
import React from "react";

events.on("canedit.html", function (context) {
    if (context._editor.priority < 10 ) {
        context._editor.priority = 10;
        context._editor.fn = () => (
            <div
                className=" html-component"
                dangerouslySetInnerHTML={{__html: context.content}}
            >

            </div>
        );
    }
});
