import {events} from "../../lib/editors";
import React, {useState} from "react";
import get from "lodash/get";
import set from "lodash/set";
import mobiscroll from "@mobiscroll/react";

events.on("canedit.bool", function (context) {
    if (context._editor.priority < 1) {
        context._editor.priority = 1;
        const [value, setValue] = useState(get(context._editor.state, context.name));
        context._editor.fn = () => (
            <mobiscroll.Checkbox defaultChecked={value} onChange={event => {
                let value = event.target.value;
                set(context._editor.state, context.name, value);
                setValue(value);
            }}>{context.question}</mobiscroll.Checkbox>
        );
    }
});

