import {events} from "../../lib/editors";
import React from "react";
import mobiscroll from "@mobiscroll/react";
import useFormState from "../../forms/form-state";

events.on("canedit.date", function (context) {
    if (context._editor.priority < 1) {
        context._editor.priority = 1;
        const [value, setValue] = useFormState(context);
        context._editor.fn = () => (
            <label className="mbsc-input">
                <span className="mbsc-label">{context.question}</span>
                <mobiscroll.Date value={value} onChange={event => setValue(event.valueText ? new Date(event.valueText) : "")}/>
            </label>

        );
    }
});

