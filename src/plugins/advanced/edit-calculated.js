import {events} from "../../lib/editors";
import React from "react";
import template from "lodash/template";

events.on("canedit.calculated", function (context) {
    if (context._editor.priority < 1) {
        context._editor.priority = 1;
        context._editor.fn = () => {
            let calculate = context._editor.reference.calculate = context._editor.reference.calculate || template(context.template);
            try {
                return context.template && (
                    <div className="mbsc-input html-component not-editable">{calculate(context._editor.state)}</div>
                );
            } catch(e) {
                return null;
            }
        };
    }
});

