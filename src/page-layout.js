import React from "react";
import mobiscroll from "@mobiscroll/react";

export default function PageLayout(props) {
    return (
        <div className="page-layout">
            <mobiscroll.Form theme="ios">
                <div className="mbsc-grid mbsc-no-padding">
                    <div className="mbsc-row mbsc-justify-content-center">
                        <div className="mbsc-col-sm9 mbsc-col-md-7 mbsc-col-xl-5">
                            {props.children}
                        </div>
                    </div>
                </div>
            </mobiscroll.Form>
        </div>
    );
}
