import {useState} from "react";

export default function wait(promise) {
    const [data, setData] = useState(promise);
    data && data.then && (async()=>{
        setData(await data);
    })();
    return !data.then ? data : null;
}
