import async from "./async";
import {EventEmitter2 as Events} from "eventemitter2";

const LOCATION = "http://localhost:3001/editors";
const events = new Events({wildcard: true});

let editors = [];
export {editors, events};

export default function bootstrap() {
    return async(async () => {
        let listOfEditors = await (await fetch(LOCATION)).json();
        let editors = await Promise.all(listOfEditors.map(e => {
            console.log(e);
            return import(`../plugins/${e}`);
        }));
        return editors;
    });
}
